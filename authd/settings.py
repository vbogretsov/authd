# -*- coding:utf-8 -*-
"""Authd settings definitions.
"""
from cfg import schema

LOG_LEVELS = {"NOTSET", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"}

LOG_CFG = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "standard": {
            "class": "logging.Formatter",
            "format": "%(asctime)s %(name)s [%(levelname)s]: %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S"
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "standard",
            "level": "DEBUG"
        },
        "file": {
            "class": "logging.handlers.RotatingFileHandler",
            "formatter": "standard",
            "level": "DEBUG",
            "filename": "",
            "maxBytes": 1048576,
            "backupCount": 10
        }
    },
    "loggers": {
        "": {
            "handlers": ["file", "console"],
            "level": "",
            "propagate": True
        }
    }
}

LOGGING_SCHEMA = schema.Schema({
    schema.Required(
        "dir",
        cli=("--log-dir",),
        default="/tmp",
        description="Log files directory."):
    str,
    schema.Required(
        "level", ("-l", "--log-level",),
        default="INFO",
        description="Log level."):
    schema.In(LOG_LEVELS)
})

DATABASE_SCHEMA = schema.Schema({
    schema.Required(
        "datadir",
        cli=("--data-dir",),
        description="Data directory."):
    str
})

CONF_SCHEMA = schema.Schema({
    schema.Required("action_ttl"): schema.TimeSpan(),
    schema.Required("token_ttl"): schema.TimeSpan(),
    schema.Required("refresh_ttl"): schema.TimeSpan(),
    schema.Optional("logging", default={"dir": "/tmp", "level": "INFO"}):
    LOGGING_SCHEMA,
    schema.Required("database"):
    DATABASE_SCHEMA
})
