# -*- coding:utf-8 -*-
"""Data access layer.
"""
import peewee
import peeweext

from authd import models

REPOSITORIES = {
    "users": lambda manager: peeweext.Repository(models.User, manager),
    "actions": lambda manager: ActionRepository(models.Action, manager)
}


def initdb(conf):
    """Craete database tables required.

    Args:
        conf (dict): Database connection settings.
    """
    database = peeweext.connect("postgresqlpool", **conf)
    models.PROXYDB.initialize(database)
    database.create_tables(
        [models.User, models.Confirmation, models.PasswordReset], safe=True)
    database.close()


def connectdb(conf, loop=None):
    """Open database connection.

    Args:
        conf (dict): Configuration dictionary.
        loop: ayncio event loop.

    Returns: dabatase, storage.
    """
    database = peeweext.connect(conf.database.driver, conf.database.dbname,
                                **conf.database.connection)
    database.allow_sync = False
    models.PROXYDB.initialize(database)
    storage = peeweext.Storage(database, REPOSITORIES, loop)
    return database, storage


class ActionRepository(peeweext.Repository):
    """Actions repository.
    """

    async def get(self, action_id):
        """Get action with by id.

        Args:
            action_id (uuid.UUID): Action id.

        Returns:
            models.Action: If confirmation with the id provided was found
                otherwise None.
        """
        try:
            return await self.manager.get((self.mapping.select(
                self.mapping.id, self.mapping.kind, self.mapping.data,
                self.mapping.expires, models.User).join(models.User).where(
                    self.mapping.id == action_id)))
        except peewee.DoesNotExist:
            return None
