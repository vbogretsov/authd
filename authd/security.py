# -*- coding:utf-8 -*-
"""Security utils.
"""
import uuid

import bcrypt
import tokenlib


def generate_id():
    """Generate random unique id.
    """
    return str(uuid.uuid4())


def hashpw(string):
    """Hash the string provided.

    Args:
        string (str): UTF-8 string to be hashed.

    Returns:
        str: Hash of the string provided.
    """
    rehash = bcrypt.hashpw(string.encode("utf-8"), bcrypt.gensalt())
    return rehash.decode("utf-8")


def checkpw(raw, hashed):
    """Check whether the raw password matches the hashed password.

    Args:
        raw (str): Raw pawssword.
        hashed (str): Salted and hashed password.
    """
    return bcrypt.checkpw(raw, hashed)


def make_token(data, secret):
    """Generate token for the data provided.

    Args:
        data (dict): Data to be wrapped in token.
        secret (str): Secret key.

    Returns:
        str: The token generated.
    """
    return tokenlib.make_token(data, secret=secret)


def parse_token(token, secret):
    """Parse the token provided.

    Args:
        token (str): The token to be parsed.
        secret (str): Secret key.

    Returns:
        dict: The token data parsed.
    """
    return tokenlib.parse_token(token, secret=secret)
