# -*- coding:utf-8 -*-
import datetime

from authd import security

CONFIRMATION = "confirmation"
PASSWORDRESET = "passwordreset"


class AuthError(Exception):
    """Base authentication error.
    """
    pass


class NotFound(AuthError):
    """Occurs when an object was not found.
    """
    pass


class Duplicated(AuthError):
    """Occurs when trying to create object that already exists.
    """
    pass


class Expired(AuthError):
    """Occurs when an object has been expired.
    """
    pass


class ActionManager:
    """Manager for delayed actions that require confirmations.
    """

    def __init__(self, conf, storage, sendmail):
        self.conf = conf
        self.storage = storage
        self.sendmail = sendmail

    async def create(self, kind, data):
        """Create unkonfirmed action.

        Args:
            kind (str): Kind of action.
            data (dict): Action data.

        Returns:
            dict: The new unkonfirmed action.
        """
        action = {
            "kind": kind,
            "data": data,
            "expires": datetime.datetime.now() + self.conf.action_ttl
        }

        async with self.storage.atomic() as atomic:
            await atomic.actions.create(action)
            await self.sendmail(
                kind,
                to=[action["data"]["email"]],
                confirmation_id=action["id"])

        return action

    async def find(self, act_id):
        """Find the action with the id provided.

        Args:
            act_id (str): Id of the action desired.

        Returns:
            dict: The action with the id provided.

        Rasies:
            NotFound: If action was not found.
            Expired: If action has been expired.
        """
        action = await self.storage.actions.find(act_id)

        if action is None:
            raise NotFound(act_id)

        if action.expires > datetime.datetime.now():
            async with self.storage.atomic():
                await self.create(action["type"], action["data"])
                await self.delete(action["id"])
            raise Expired(act_id)

        return action

    async def delete(self, act_id):
        """Delete the action with the id provided.

        Delete occurs in transaction so this operation can be a part of other
        transaction.

        Args:
            act_id (str): Id of the action desired.

        Returns:
            True: If action was deleted.
            False: If action with the id provided was not found.
        """
        async with self.storage.atomic() as atomic:
            return await atomic.action.delete(act_id)


class UserManager:
    """Manager for users an theirs credentials.
    """

    def __init__(self, conf, storage, action_manager):
        self.conf = conf
        self.storage = storage
        self.action_manager = action_manager

    async def create(self, email, password):
        """Create new user.

        New user will be created but not allowed for login. The user
        confirmation information will be sent to the email provided.

        Args:
            email (str): Email of the new user.
            password (str): Password of the new user.
        """
        if await self.storage.users.find(email):
            raise Duplicated(email)

        user_data = {
            "email": email,
            "password": security.hashpw(password),
            "id": security.generate_id(),
            "active": False
        }
        action_data = {"email": email}

        async with self.storage.atomic() as atomic:
            await atomic.users.create(user_data)
            await self.action_manager.create(CONFIRMATION, action_data)

    async def confirm(self, confirmation_id):
        """Confirm a user using the confirmation id provided.

        Args:
            confirmation_id (str): Id of the confirmation that should be used
                to activate the user.

        Raises:
            NotFound: If confirmation with the id provided was not found.
            Expired: If confirmation with the id provided has been expired.
        """
        confirmation = await self.action_manager.find(confirmation_id)

        async with self.storage.atomic() as atomic:
            await atomic.users.update(confirmation.data.email, active=True)
            await self.action_manager.delete(confirmation_id)

    async def delete(self, email):
        """Delete the user with the email provided.

        Delete occurs in transaction so this operation can be a part of other
        transaction.

        Args:
            email (str): Email of the user to be deleted.

        Raises:
            NotFound: If the user with the email provided does not exist.
        """
        async with self.storage.atomic() as atomic:
            if not await atomic.users.delete(email):
                raise NotFound(email)

    async def reset_password(self, email):
        """Request password reset.

        Password reset information will be sent the email provided.

        Args:
            email (str): Email of the user which password should be updated.

        Raises:
            NotFound: If the user with the email provided does not exist.
        """
        user = await self.storage.users.find(email)

        if user is None:
            raise NotFound(email)

        await self.action_manager.create(PASSWORDRESET, {"email": email})

    async def update_password(self, request_id, password):
        """
        Update user's password.

        Args:
            request_id (str): Password reset request id.
            password (str): New password.

        Raises:
            NotFound: If request with the id provided was not found or the user
                with the email from request does not exist.
            Expired: If request with the id provided has been expired.
        """
        request = await self.action_manager.find(request_id)
        user = await self.storage.users.find(request.data.email)

        if user is None:
            raise NotFound(request.data.email)

        async with self.storage.atomic() as atomic:
            await atomic.users.update(
                user.id, password=security.hashpw(password))
            await self.action_manager.delete(request_id)


class TokenManager:
    """Access and refresh tokens manager.
    """

    def __init__(self, secret, storage):
        self.secret = secret
        self.storage = storage

    async def login(self, email, password):
        user = await self.storage.users.find(email)

        if user is None or not security.checkpw(password, user.password):
            return None

        data = {
            "user_email": user.email,
            "user_id": user.id,
            "token_id": security.generate_id(),
            "expires": datetime.datetime.now() + self.conf.token_ttl
        }

        async with self.storage.atomic() as atomic:
            await atomic.tokens.create(data)

        return security.make_token(data, self.secret)

    async def resfresh(self):
        pass

    async def refuse(self):
        pass
