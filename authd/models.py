# -*- coding:utf-8 -*-
"""Data model definitions.
"""
import uuid

import peewee
import peeweext
import wrapt


class ProxyDb(wrapt.ObjectProxy):
    """Database proxy to allow initialize database later from config.

    We don't use peewee.Proxy because peewee_async does not support it.
    """

    def initialize(self, database):
        """Initialize database.
        """
        self.__wrapped__ = database


PROXYDB = ProxyDb(None)


class Entity(peewee.Model):
    """Base class for database entity.
    All account entities shoud be derived from this class.
    """
    id = peewee.UUIDField(primary_key=True, default=uuid.uuid4)

    class Meta:
        """Peewee Meta.
        """
        database = PROXYDB


class User(Entity):
    """Represents an account record.
    """
    email = peewee.CharField(unique=True, index=True)
    password = peewee.CharField(null=False)
    active = peewee.BooleanField(null=False, default=False)
    created = peewee.database(null=False)


class Action(Entity):
    """Represents an account confirmation record.
    """
    kind = peewee.CharField(null=False)
    data = peeweext.JsonField(null=False)
    expires = peewee.DateTimeField(null=False)
    user = peewee.ForeignKeyField(User, on_delete="CASCADE")
