# -*- coding:utf-8 -*-
"""A setuptools based setup module for the maild service.
"""

import setuptools


GITLAB_URL = "git+https://gitlab.com/vbogretsov"
DEPENDENCY = "{0}/{1}.git#egg={1}-{2}"

setuptools.setup(
    name="authd",
    version="0.1.0",
    description="Simple token based authentication server.",
    url="https://gitlab.com/vbogretsov/authd",
    author="vbogretsov",
    author_email="bogrecov@gmail.com",
    license="BSD",
    packages=["authd"],
    py_modules=["authctl"],
    setup_requires=[
        "pytest-runner"
    ],
    install_requires=[
        "aiorfc",
        "attrdict",
        "bcrypt",
        "cfg",
        "click",
        "kvstore",
        "tokenlib",
        "peewee",
        "peewee-async",
        "peeweext",
        "voluptuous",
        "wrapt"
    ],
    tests_require=[
        "pytest==3.0.7",
        "pytest-asyncio",
        "pytest-cov",
        "pytest-mock",
        "pytest-aioamqp"
    ],

    entry_points="""
        [console_scripts]
        authd=authctl:start
    """,
    dependency_links=[
        DEPENDENCY.format(GITLAB_URL, "aiorfc", "0.2.0"),
        DEPENDENCY.format(GITLAB_URL, "cfg", "0.1.0"),
        DEPENDENCY.format(GITLAB_URL, "kvstore", "0.1.0"),
        DEPENDENCY.format(GITLAB_URL, "peeweext", "0.1.0"),
        DEPENDENCY.format(GITLAB_URL, "pytest-aioamqp", "0.1.0")
    ]
)
